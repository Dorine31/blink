#include <Arduino.h>

#define LED LED_BUILTIN
#define LED_MASK (1 << 5)
#define CS_PRESCALER_1024 ((1 << CS10) | (1 << CS12))
#define TICKS_IN_S 15625

void timer_setup() {
  TCCR1A = 0; // WGM11 et WGM10 à 0
  TCCR1B =  CS_PRESCALER_1024 | (1 << WGM12);
  OCR1A = TICKS_IN_S;
  TIMSK1 = (1 << OCIE1A);
}

void led_setup() {
  DDRB |= LED_MASK;  
}

void led_toggle() {
  PINB = LED_MASK;  // fait basculer l'état sur un registre independant du registre portB
}

// INTERRUPT SERVICE ROUTING
ISR(TIMER1_COMPA_vect){
  led_toggle();
}

void setup() {
  led_setup();
  timer_setup();
  Serial.begin(9600);
  Serial.println("Blink");
}

void loop() {
  // test de l'arret du delay à la mise à zero
  delay(10000);
  TCCR1B &= ~(CS_PRESCALER_1024);
  delay(10000);
  TCCR1B |= CS_PRESCALER_1024;
}